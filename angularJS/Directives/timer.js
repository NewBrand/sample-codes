'use strict';

/**
 * @ngdoc directive
 * @name newConfigsApp.directive:timer
 * @description
 * # timer
 */
angular.module('newConfigsApp')
  .directive('timer', function ( $interval) {
    return {
      restrict: 'E',
      scope: {
        time: '@time'
      },
      link: function($scope,element,attributes) {
         var timer='';
          
        $scope.$watch('time',function(newVal,oldVal){
            
          var time=newVal;
 
        if(timer){
            
            $interval.cancel(timer);
        }
        
        var nowTime = new Date();
        nowTime = nowTime.getTime() + 60 * 60 * 1000;
        var diff = Math.floor((nowTime - time) / 1000);
     
        function updateTimer() {
          if(time){
            timer= $interval(function () {
              diff++;
              $scope.minutes = Math.floor(diff / 60);
              $scope.seconds = diff % 60;
             
              }, 1000);
          };
        }
      
        updateTimer();
       


      
     
      
        });
      element.on('$destroy', function() {
        
            $interval.cancel(timer);
          });
       

      },
   
      template: '{{minutes}}:{{seconds>9?seconds:"0"+seconds}}',
    };
  });

'use strict';

/**
 * @ngdoc service
 * @name newConfigsApp.Todo
 * @description
 * # Todo
 * Service in the newConfigsApp.
 */
angular.module('newConfigsApp')
  .service('Todo', function ($http, $rootScope, $timeout, User, toastr, Stats) {

    this.content = {
      toDoList: {
        list: {},
        generalCount: 0
      },
      activeTask: {
        task: null
      }


    };

    var that = this;

    function serializeTime(task) {
      var dateArray = task.startedAt.match(/\d{2,}/g);

      if (dateArray) {
        task.startedAtUnix = Date.UTC(dateArray[2], dateArray[1] - 1, dateArray[0], dateArray[3], dateArray[4], dateArray[5]);
      }
      return task;
    }

    this.getTodo = function () {


      var promise = $http({
        method: 'GET',
        url: $rootScope.apiUrl + 'todolist/' + User.data.info.name
      });

      $rootScope.loadings.todo = true;
      promise.then(function (response) {
     

        that.content.toDoList.list = response.data;
        that.content.toDoList.generalCount = response.data.auctions.count + response.data.descriptionsExt.count;

        $rootScope.loadings.todo = false;
      }, function (response) {
        $rootScope.loadings.todo = false;
      });




    };
    this.setActiveTask = function (task) {
      if (task.account) {
        task.from = 'Aukcje';
      } else {
        task.from = 'Opisy rozszerzone';
      }


      if (task.status === "W trakcie") {
        this.content.activeTask.task = serializeTime(task);
      } else {
        this.content.activeTask.task = task;
      }



    };
    this.clearActiveTask = function () {

      that.content.activeTask.task = null;

    };
    this.setStatus = function (status) {

      var statusName = null;

      switch (status) {
        case 'inProgress':
        case 'done':
        case 'waiting':
        case 'reset':
          statusName = status;
          break;

      }
      if (statusName) {
        this.content.activeTask.task.user_id = User.data.info.id;
        $rootScope.loadings.activeTask = true;
        var promise = $http({
          method: 'POST',
          url: $rootScope.apiUrl + 'todolist/set/' + statusName,
          data: this.content.activeTask.task
        });

        promise.then(function (msg) {
          var activeTask = that.content.activeTask.task;
          switch (msg.data.info.status) {

            case 'started':
              activeTask.status = "W trakcie";
              activeTask.startedAt = msg.data.info.data;
              activeTask = serializeTime(activeTask);
              that.content.activeTask.task = activeTask;
              toastr.success('Rozpoczęto realizacje zadania');
              break;
            case 'done':
              that.clearActiveTask();
              Stats.completedTask(activeTask, msg.data.stats);
              break;
            case 'waiting':
              that.clearActiveTask();
              break;
            case 'error':
              toastr.error('Prawdopodobnie nie udało się znaleźc zadania');
              break;
            case 'reset':
              that.clearActiveTask();
              toastr.info('Wyzerowano liczniki');
              break;
            default:
              break;

          }
          $rootScope.loadings.activeTask = false;
          that.getTodo();
          statusName = null;
        });
      }


    };



   


  });

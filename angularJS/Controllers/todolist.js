'use strict';

/**
 * @ngdoc function
 * @name newConfigsApp.controller:TodolistCtrl
 * @description
 * # TodolistCtrl
 * Controller of the newConfigsApp
 */
angular.module('newConfigsApp')
  .controller('TodolistCtrl', function ($scope, Todo) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    $scope.toDo = Todo.content.toDoList;
    $scope.filters = {
      search: '',
      sort: {
        label: 'Data: Najnowsze',
        name: 'time',
        reverse: false
      }
    };
    Todo.getTodo();
    $scope.isTaskCollapsed = [];
    $scope.toDoCarousel = {};
    $scope.toDoCarousel.active = 0;
    $scope.setTaskCollapsed=function(parent,index){
      
   
        $scope.isTaskCollapsed[parent][index]=!$scope.isTaskCollapsed[parent][index];
    }
    $scope.statusClass = function (status) {

      switch (status) {
        case 'W trakcie':
          return '_inProgress';
          break;
        case 'Oczekujące':
          return '_awaiting';
          break;
        case 'Gotowe do wykonania':
          return '_ready';
          break;
         case 'Do poprawy':
          return '_forFix';
          break;

        default:
          return '';
          break;
      }

    }
    $scope.taskTypeIcon = function (type) {

      switch (type) {
        case 'Edycja':
          return 'fa-pencil-square-o';

        case 'Nowa aukcja':
        case 'Nowy opis rozszerzony':
          return 'fa-plus-square';
        case 'na bazie':
        case 'Na bazie':
        case 'Nowa na bazie':
          return 'fa-clone';

        case 'Z aukcji':
          return 'fa-retweet';

        case 'Skompletowany':
          return 'fa-check-circle';

        case 'Wycofane/nieaktualne':
          return 'fa-trash-o';

        case 'Poprawa po weryfikacji':
          return 'fa-wrench';

        case 'Poprawa':
          return 'fa-wrench';
        case 'Okazje Allegro ':
        case 'Okazje Allegro':
          return 'fa-diamond';

        default:
          return 'fa-question-circle';
      }
    };

    $scope.taskPriorityIcon = function (priority) {

      switch (priority) {
        case '1':
          return 'fa-star'

        case '2':

          return 'fa-star-half-o'
        case '3':

          return 'fa-star-o'
        default:
          return 'fa-question-circle';
      }
    };
    $scope.serializeName = function (text) {
      if (text) {


        var serialized = text.replace(/(http:\S+)/g, function (match, contents, offset, s) {

          var site = contents.match(/\/\/[^\/]+/)[0];
          site = site.replace(/\/\//, '');
          var output = '<a href="' + contents + '" target="_blank" title="' + site + '"> ' + site + ' </a>';

          return output;
        });
        serialized = serialized.replace(/^(\d{5,6})\s+|\s(\d{5,6})\s?|\s\d{5,6}$|^\d{5,6}$/gm, function (match, contents, offset, s) {
          match = match.trim();
          var output = '<a href="http://www.x-kom.pl/p/' + match + '" target="_blank" title="znajdz na x-kom.pl"> ' + match + ' </a>';
          return output;
        });
        return serialized;
      } else {
        return '-----';
      }
    }

    $scope.setActiveTask = function (task) {
      Todo.setActiveTask(task);
    }


    function resetCollapse() {
      angular.forEach($scope.isTaskCollapsed, function (value, key) {
        angular.forEach(value, function (valueLow, key2) {

          $scope.isTaskCollapsed[key][key2] = true;
        });
      });
    };

    $scope.refreshToDo = function () {
      Todo.getTodo();
      resetCollapse();
    }
    $scope.changeOrder = function (label, name, reverse) {
      $scope.filters.sort = {
        label: label,
        name: name,
        reverse: reverse
      };
      resetCollapse();
    }


    $scope.$watch('filters.search', function () {
      resetCollapse();
    })

  });

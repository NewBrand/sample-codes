<?php namespace App\Services;

use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Config;

class GoogleDrive {
	
	protected $client;
	
	protected $service;
	
	protected $spreadsheetId='xxx';
	
	
	
	function __construct() {
		
		
		$client_id = Config::get('google.client_id');
		
		$service_account_name = Config::get('google.service_account_name');
		
		$key_file_location = base_path() . Config::get('google.key_file_location');
		
		$this->client = new \Google_Client();
		
		$this->client->setApplicationName("X-Configs");
		
		$this->client->setAuthConfig($key_file_location);
		
		$this->client->setScopes(array('https://www.googleapis.com/auth/drive'));
		
		$this->service = new \Google_Service_Sheets($this->client);
		
		Cache::forever('service_token', $this->client->getAccessToken());
		
	}
	
	
	/*
	*
	* checking is task performed by our user and its status is different from ended
	* if name is blank checking for all not ended tasks
	*
	*/

	
	private function isOwner($row,$name){
		
		

		if ($name!='') {
			
			return $row['performer']==$name && $row['status']!='Zakonczone';
			
		}
		
		else {
			
			return $row['status']!='Zakonczone';
			
		}
		
	}
	
	/*
	*
	*	return all tasks from specified sheet in array with labels
	*
	*/
	private function getFeed($sheet=''){
		
		$feed=[];
		
		switch ($sheet) {
			
			case 'descriptionsExt':
				$range = 'Opisy rozszerzone!A1:P';
				
				$labels=[
				'id',				//LP
				'time',				//Data/godzina
				'category',			//Kategoria
				'producer',			//Producent
				'pm',				//PM/aPM
				'item',				//Model (Kod producenta, ID)
				'numberOfActions',	//Liczba SKU
				'priority',			//Priorytet
				'keyPoints',		//Key Points
				'comments',			//Uwagi
				'technical',		//kolumna techniczna nie kasować
				'performer',		//Wykonuje
				'taskType',			//Typ zadania
				'status',			//Status
				'endedAt',			//Data i godzina rozpoczęcia
				'startedAt',		//Data/godzina wystawienia
				
				];
				
				break;
			
			default:
				$range = 'Aukcje!A1:Y';
				
				$labels=[
				'id',				//LP
				'time',				//Data/godzina
				'category',			//Kategoria
				'producer',			//Producent
				'pm',				//PM / aPM
				'performer',		//Wykonuje
				'account',			//Konto
				'priority',			//Priorytet
				'taskType',			//Typ zadania
				'auctionType',		//Typ aukcji
				'item',				//Model (Kod producenta, ID)lub link do aukcji
				'price',			//Wersja / Cena
				'numberOfActions',	//Liczba aukcji do wystawienia / edycji
				'numberOfPieces',	//Liczba sztuk na aukcji
				'startTime',		//Data/godzina wystawienia
				'promotionType',	//Promowanie
				'deliveryType',		//Wysyłka
				'set',				//W zestawie
				'blank',			//Blank
				'keyPoints',		//Key Points
				'comments',			//Uwagi
				'status',			//Status
				'endedAt',			//Data i godzina rozpoczęcia
				'startedAt',		//Data/godzina wystawienia
				'descriptionsCount'	//Opis rozszerzony 
				];
				
				break;
			
		}
		
		$response = $this->service->spreadsheets_values->get($this->spreadsheetId, $range);
		
		$values = $response->getValues();
		
		foreach ($values as $value) {
			
			$row=[];

			//setting label for each cell
			
			for ($i=0; $i < count($labels) ; $i++) {
			
				$row[$labels[$i]]=(isset($value[$i])) ? $value[$i] : '';
				
			}
			
			$feed[]=$row;
			
		}
		
		return $feed;
		
	}
	
	/*
	*
	* get not ended task from all sheets and their counts
	*
	*/

	public function getTasks($name="")
	{
		$output=array();
		$output['auctions']['list'] = array_filter($this->getFeed(),function($array) use ($name){
			
			return $this->isOwner($array,$name);
			
		}
		
		);
		
		$output['auctions']['list']=array_values($output['auctions']['list']);
		
		$output['auctions']['count']=count($output['auctions']['list']);
		
		$output['descriptionsExt']['list'] = array_filter($this->getFeed('descriptionsExt'),function($array) use ($name){
		
			return $this->isOwner($array,$name);
			
		}
		
		);
		
		$output['descriptionsExt']['list']=array_values($output['descriptionsExt']['list']);
		
		$output['descriptionsExt']['count']=count($output['descriptionsExt']['list']);
		
		return $output;
		
	}


	/*
	*
	* Function which comapare task
	*
	*/


	private function toDoFilter($source,$filter){
		
		foreach ($filter as $key => $value) {
			
			if($source[$key]!=$value){
				
				return false;
				
			}
			
		}
		
		return true;
		
	}

	/*
	*
	* Function which return index of task in sheet
	*
	*/

	private function getIndex($sheetName,$filters){
	
		switch($sheetName){
				
				case "Opisy rozszerzone":
				$feed=$this->getFeed('descriptionsExt');
				
				break;
				
				case "Aukcje":
				$feed=$this->getFeed();
				
				break;
				
			}
			
			$index='';
			
			for ($i=0;$i<count($feed);$i++){
				
				if($this->toDoFilter($feed[$i],$filters)){
					
					$index=$i+1;
					
					break;
					
				}
				
			}
			if($index){
				return $index;
			}else{
				return false;
			}
	}

	/*
	*
	* Function which change task status in sheet
	*
	*/
	public function setStatus($sheetName="Aukcje",$filters,$status='',$extraData=array()){
		
		
		$index = $this->getIndex($sheetName,$filters);

		if($index){


		
		$data = array();
		
		
		
		$stringDate=date('d/m/Y H:i:s');
		$operation='';
		$columnIndex=array();

		switch ($sheetName) {
			case 'Aukcje':
				$columnIndex=[
					'startedAt'=>'X',
					'endedAt'=>'W',
					'descriptionsCount'=>'Y',
					'comments'=>'U',
					'status'=>'V'
				];
				break;
			
			case 'Opisy rozszerzone':
				$columnIndex=[
					'startedAt'=>'P',
					'endedAt'=>'O',
					'comments'=>'J',
					'status'=>'N'
				];
				break;
		}


		switch ($status) {
			
			case 'W trakcie':
			$values_timeNow = array(
				array(
					$stringDate
				),
			);
			
			$data[] = new \Google_Service_Sheets_ValueRange(
				array(
					'range' => $sheetName.'!'.$columnIndex['startedAt'].$index,
					'values' => $values_timeNow
				)
			);
			$operation='started';
			break;
			
			case 'Zakonczone':
				$values_timeNow = array(
					array(
						$stringDate
					),
				);
				
				$data[] = new \Google_Service_Sheets_ValueRange(
					array(
						'range' => $sheetName.'!'.$columnIndex['endedAt'].$index,
						'values' => $values_timeNow
					)
				);
				
				if(isset($extraData['descriptionsCount'])){
					$values_timeNow = array(
						array(
							$extraData['descriptionsCount']
						)
					);
					
					$data[] = new \Google_Service_Sheets_ValueRange(
						array(
							'range' => $sheetName.'!'.$columnIndex['descriptionsCount'].$index,
							'values' => $values_timeNow
						)
					);
					
				}
	
			$operation='done';
			break;
			
			case 'Oczekujące':
				$values_time0 = array(
					array(
						''
					)
				);
				
				$data[] = new \Google_Service_Sheets_ValueRange(
					array(
						'range' => $sheetName.'!'.$columnIndex['startedAt'].$index,
						'values' => $values_time0
					)
				);
				$data[] = new \Google_Service_Sheets_ValueRange(
					array(
						'range' => $sheetName.'!'.$columnIndex['endedAt'].$index,
						'values' => $values_time0
					)
				);

				if($extraData['comments']){
					$values_comments = array(
						array(
							$extraData['comments']
					)
				);
					$data[] = new \Google_Service_Sheets_ValueRange(
						array(
							'range' => $sheetName.'!'.$columnIndex['comments'].$index,
							'values' => $values_comments
					)
				);
				}
				$operation='waiting';
			break;
			
			case 'reset':
				$status='';
				$values_time0 = array(
					array(
						''
					)
				);
				
				$data[] = new \Google_Service_Sheets_ValueRange(
					array(
						'range' => $sheetName.'!'.$columnIndex['startedAt'].$index,
						'values' => $values_time0
					)
				);
				$data[] = new \Google_Service_Sheets_ValueRange(
					array(
						'range' => $sheetName.'!'.$columnIndex['endedAt'].$index,
						'values' => $values_time0
					)
				);
				$operation='reset';
			break;
			
		}
		$values_status = array(
			array(
				$status
			),
		);
		
		$data[] = new \Google_Service_Sheets_ValueRange(
			array(
				'range' => $sheetName.'!'.$columnIndex['status'].$index,
				'values' => $values_status
			)
		);
		
		$body = new \Google_Service_Sheets_BatchUpdateValuesRequest(
			array(
				'valueInputOption' => 'USER_ENTERED',
				'data' => $data
			)
		);
		
		try{
			$this->service->spreadsheets_values->batchUpdate($this->spreadsheetId, $body);

			return array('status'=>$operation,'data'=>$stringDate);
		}catch(\Exception $e){
			return ['status'=>'error','data'=>$e];
		}
		
		
		
		}else {
			return ['status'=>'error','data'=>'wpis nie został znaleziony','errorExt'=>$filters];
		}
	}

	/*
	*
	* Function which get scores from sheet 
	*
	*/
	
	public function getScores($lastIndex_1=76,$lastIndex_2=84)
	{
		$response_1 = $this->service->spreadsheets_values->get($this->spreadsheetId, 'Punktacja!A3:G'.$lastIndex_1);
		
		$values_1 = $response_1->getValues();

		$response_2 = $this->service->spreadsheets_values->get($this->spreadsheetId, 'Punktacja!A79:b'.$lastIndex_2);
		
		$values_2 = $response_2->getValues();

		return [
			'data'=>$values_1,
			'extra'=>$values_2
		];
	}
	
}


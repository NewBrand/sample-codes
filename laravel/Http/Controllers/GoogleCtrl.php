<?php

namespace App\Http\Controllers;
use App\Services\GoogleDrive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class GoogleCtrl extends Controller
{
	
	public static function getToDoList(GoogleDrive $sheet, $name=''){
		
		return $sheet->getTasks($name);
	}
	
	public static function setStatus(GoogleDrive $sheet, Request $request, $status){
		$input=$request->all();
		
		$filters=array(
					'time' =>$request->time,
					'category'=>$request->category,
					'pm'=>$request->pm,
					'item'=>$request->item,
					'performer'=>$request->performer,
				);
		$extraData=[];
		
		if($request->from=='Aukcje'){
			$extraData['descriptionsCount']=$request->descriptionsCount;
		}
		if($status=="waiting"){
			$extraData['comments']=$request->comments;
		}
		;
		
		switch ($status) {
			case 'inProgress':
                $statusName="W trakcie";
			break;
            case 'waiting':
                $statusName="Oczekujące";
			break;
            case 'done':
                $statusName="Zakonczone";
			break;
			case 'reset':
				$statusName='reset';	                
			break;
		}
		
		$info=$sheet->setStatus($request->from,$filters,$statusName,$extraData);
		$output=[];
		$output['info']=$info;
		$stats='';
		if( $statusName=="Zakonczone"){
			$output['stats']=StatController::calculateStat('task',$input,$info['data']);
		}

		return $output;
		
	}
	
}

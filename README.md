# CODE SAMPLES

A few code samples for recruitment purposes

## Laravel

A sample controller and driver for fetching and editing data in google sheets. Driver is not extended by the interface (although it should) because Google sheet will be probably the only way to store tasks

## AngularJS

Controller, service and view to fetch, store and show tasks from Google sheets. Also one directive with timer